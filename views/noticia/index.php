<?php

use app\models\Noticia;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticia-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Noticia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idNoticia',
            'titular',
            'textoCorto',
            //'textoLargo:ntext',
            //'portada',
            // 'seccion'
            [
                'attribute' => 'seccion',
                'value' => function ($model) {
                    return $model->seccion0->nombre;
                }
            ],
            //'fecha',
            //'foto',
            //'autor',
            // forma de colocar una imagen en GRIDVIEW
            [
                'attribute' => 'foto',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img('@web/imgs/' . $model->foto, ['class' => 'col-lg-2']);
                }
            ],

            // modificar la presentacion del campo portada
            [
                'attribute' => 'portada',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->portada == 1 ? '<i class="fas fa-check-square"></i>' : '<i class="fas fa-times-square"></i>';
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Noticia $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idNoticia' => $model->idNoticia]);
                }
            ],
        ],
    ]); ?>


</div>