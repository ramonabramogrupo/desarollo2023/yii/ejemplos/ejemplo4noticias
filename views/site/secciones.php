<?php

use yii\helpers\Html;


?>
<div class="jumbotron text-center bg-transparent mt-5 mb-5">
    <h1 class="display-4">Secciones</h1>
</div>

<div class="row">
    <?php
    foreach ($secciones as $seccion) {
    ?>
        <div class="col-4 mt-3">
            <div class="card bg-white text-dark">
                <?= Html::img('@web/imgs/secciones/' . $seccion->foto, ['class' => 'm-2']) ?>
                <div class="card-body">
                    <h5 class="card-title"><?= $seccion->nombre ?></h5>
                    <?= Html::a('ver noticias', ['site/seccion', 'id' => $seccion->id], ['class' => 'btn btn-secondary']) ?>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>